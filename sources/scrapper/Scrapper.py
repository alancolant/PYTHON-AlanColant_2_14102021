import csv
import os

import requests
from bs4 import BeautifulSoup


# Class descriptive categorie -> livre
class Category:
    def __init__(self):
        self.name: str = None
        self.link: str = None

    def __repr__(self):
        return self.name + ' - ' + self.link


class Book:
    def __init__(self):
        self.product_page_url: str = None
        self.universal_product_code: str = None
        self.title: str = None
        self.price_including_tax: float = None
        self.price_excluding_tax: float = None
        self.number_available: int = None
        self.product_description: str = None
        self.category: Category = None
        self.review_rating: float = None
        self.image_url: str = None

    def __repr__(self):
        return str(self.__dict__)


# Extraction
def get_remote_content_parsed(url: str) -> BeautifulSoup:
    page: bytes = requests.get(url).content
    return BeautifulSoup(page, 'html.parser')


def join_url(base_url: str, link: str) -> str:
    return requests.compat.urljoin(base_url, link)


# Transformation-Chargement
class Scrapper:
    def __init__(self, url: str):
        self.__base_url = url

    @property
    def base_url(self):
        return self.__base_url

    def get_categories(self) -> list[Category]:
        parser = get_remote_content_parsed(self.__base_url)
        categories: list = []
        for li in parser.find("div", class_="side_categories").find('ul').find('ul').find_all('li'):
            category = Category()
            category.name = li.find('a').text.strip()
            category.link = li.find('a')['href']
            categories.append(category)
        return categories

    def get_books(self, category: Category, from_page: int = 1) -> list[Book]:
        print("Fetch page " + str(from_page) + " from " + category.name)
        # Generate link
        link: str = join_url(self.__base_url, category.link)
        if from_page > 1:
            link = link.replace('index.html', 'page-' + str(from_page) + '.html')
        # Fetch page
        parser = get_remote_content_parsed(link)
        books: list[Book] = []

        for article in parser.find_all('article', class_="product_pod"):
            books.append(self.__generate_book(article, category, link))

        # Detect if next page exists
        try:
            has_next_page = len(parser.find('ul', class_="pager").find('li', class_='next').find('a')['href']) > 0
        except AttributeError:
            # There is no next page
            has_next_page = False

        # Recursive insert books from other pages
        if has_next_page:
            books += self.get_books(category, from_page + 1)
        return books

    def __generate_book(self, article, category, link) -> Book:
        book: Book = Book()
        # From listing
        book.category = category
        book.product_page_url = join_url(link, article.find('a')['href'])
        book.image_url = join_url(link, article.find('a').find('img', class_="thumbnail")['src'])
        book.review_rating = article.find('p', class_="star-rating")['class']
        # Replace rating with value
        if 'One' in book.review_rating:
            book.review_rating = 1
        elif 'Two' in book.review_rating:
            book.review_rating = 2
        elif 'Three' in book.review_rating:
            book.review_rating = 3
        elif 'Four' in book.review_rating:
            book.review_rating = 4
        elif 'Five' in book.review_rating:
            book.review_rating = 5
        else:
            book.review_rating = 0
        # From book page
        book_parser = get_remote_content_parsed(join_url(link, article.find('a')['href']))
        book.title = book_parser.find('h1').text.strip()

        try:
            book.product_description = book_parser.find('div', id='product_description').find_next('p').text.strip()
        except AttributeError:
            # There is no description
            book.product_description = ""

        # Get table
        table = book_parser.find('table', class_="table table-striped")
        book.universal_product_code = table.find('th', text='UPC').find_next('td').text.strip()
        book.price_including_tax = table.find('th', text='Price (incl. tax)').find_next('td').text.strip()
        book.price_excluding_tax = table.find('th', text='Price (excl. tax)').find_next('td').text.strip()
        book.number_available = int(
            ''.join(filter(str.isdigit, table.find('th', text='Availability').find_next('td').text.strip()))
        )
        return book

    def save_books_as_csv(self, books: list[Book], output_directory: str):
        if not os.path.exists(output_directory):
            os.makedirs(output_directory)

        with open(output_directory + '/books.csv', mode='w') as books_csv:
            csv_writer = csv.writer(books_csv, delimiter=";")
            # Write header
            csv_writer.writerow(
                ['product_page_url', 'universal_product_code', 'title', 'price_including_tax', 'price_excluding_tax',
                 'number_available', 'product_description', 'category', 'review_rating', 'image_url'])

            # Write data
            for book in books:
                csv_writer.writerow(
                    [book.product_page_url, book.universal_product_code, book.title, book.price_including_tax,
                     book.price_excluding_tax, book.number_available, book.product_description, book.category.name,
                     book.review_rating, book.image_url])
        pass

    def download_images(self, books: list[Book], output_directory: str):
        for book in books:
            response = requests.get(book.image_url, allow_redirects=True)
            if not os.path.exists(output_directory):
                os.makedirs(output_directory)
            open(output_directory + '/' + book.title.replace('/', '-') + '.jpg', 'wb').write(response.content)
