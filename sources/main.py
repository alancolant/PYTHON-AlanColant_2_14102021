import sys
import scrapper.Scrapper as s


def main():
    if len(sys.argv) > 1:
        output_dir = sys.argv[1]
    else:
        print('Veuillez renseignez un dossier de sortie')
        return

    scrapper = s.Scrapper('https://books.toscrape.com/')
    for cat in scrapper.get_categories():
        books = scrapper.get_books(cat)
        scrapper.save_books_as_csv(books, output_dir + '/' + cat.name)
        scrapper.download_images(books, output_dir + '/' + cat.name + '/images')


if __name__ == '__main__':
    main()
