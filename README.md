# Requirements

- Python 3.9.x

# Usage

## With venv

```sh
python3 -m venv /path/to/new/virtual/environment #Create venv
source /path/to/new/virtual/environment/bin/activate 

pip install -r sources/requirements.txt

cd sources
python main.py output_directory
```

## Without venv

```sh
pip install -r sources/requirements.txt
cd sources
python main.py output_directory
```